Daniel Cantos
Jake Krelle
Assignment 5 submission 1 README

Overview:
Source files for a touch screen turing machine written in python and kivy for the android OS
Includes a state diagram and tape that may be edited by touch, and buttons to control what the machine does


Getting Started:
Installation:
Unzip the source files onto your machine
1 - running from kivy .bat file:
find where you unzipped the files and drag the file main.py onto the kivy .bat file 

2 - runnin from console:
Mac/Linux: requires kivy to be installed
cd path-to-dir
kivy main.py

Usage:
the interface consists of a state diagram taking up most of the screen, a tape along the bottom, and various buttons.

create states by tapping in the orange space that is the state diagram.
the first state created is automatically an initial state. to change this, double tap another state and hit 'make initial'.
you can also change the name of the state by double tapping, though the name won't be displayed.

draw transitions by dragging between states. Note: drawing to a blank space will leave a line there but wont add a transition.
drawing a transition brings up a popup to input read, write and tape movement.
input L for left, R for right and anything else to have no movement.

change the tape by tapping on it, you change an individual cell manually.
must remove the tapehead * markers if they are there.

change the alphabet by touching the alphabet button. currently there are no alphabet checks for the tape

pan the state diagram by tapping the Edit button in the top left. don't drag a state as this is currently not working and will mess
up your state diagram. this allows you to build larger machines than the screen will hold.
pinch zooming doesn't work so don't try, may mess things up
tapping the Drag button will switch it back to edit

step the machine once by pressing the step button
run to completion by hitting the run button
change the run speed by hitting the speed button
speed number represents the steps per second


Implemented:
1. State diagram that uses touch events to create states and draw transitions
what is possible currently:
-create new state
-set initial state
-set accepting states
-draw transition and set transition values
-rename states (doesn't display though, but states are renamed)
-deleting states
-pan the entire diagram
-undo and redo MOST changes

what isn't implemented:
-delete transitions (can be overwritten and pointed to a crash state, equivalent to deleting)
-drag states (very glitchy)
-resize diagram by pinch actions (pinches are hard)
-display state names

2. Tape that can be edited
what is implemented:
-rewrite tape (this is how to write a new one as well)
-displays the tape head

what isn't implemented:
-change cell symbol
-scroll left and right

3. buttons to control machine flow
what is implemened:
-step machine by one
-reverse stepping back to start
-run machine to completion
-change run speed
-change alphabet (useless because machine doesn't check, can change blank)
-change blank (changes to first character in alphabet)
-toggle between editing the machine and moving it around

what isn't implemented:
-find head (waiting on scrollable tape)
-display the left and right tape cell numbers


Known Bugs:
Dragging a state produce weird graphical glitches. seems to put the state way offscrean
there is a segmentation fault when starting dragging a transition. Haven't been able to reproduce it, may have been unkowingly patched
apk crashes on launch
redoing states that have been undone causes an error
redoing when you've deleted a state causes an error

Limitations:
running to completion doesn't display each step along the way, hope it halts :)
it also takes up all functionality until it finishes, can't change speed once it starts

setting an alphabet is useless unless you want to change the blank symbol
cannot change the current state unless you set it to be a new state

once the tape writes past the right of the screen, the only way to view all of it is to tap on it to bring up the tape editor
if you do this, make sure you get rid of the *'s marking the tape head

run to completion has no max speed setting, don't bother trying to run a busy beaver to completion :/

panning has no boundaries, so it is possible to pan far away from the state diagram and never be able to find it again
