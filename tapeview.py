from tape import Tape

from kivy.uix.widget import Widget
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.label import Label
from kivy.uix.scrollview import ScrollView
from kivy.uix.textinput import TextInput
from kivy.uix.popup import Popup

class TapeView(ScrollView):
    "tape for the machine"
    tape = None
    "booleans to control how the tapeview behaves on touch events"
    touched = False
    
    def on_touch_down(self, touch):
        "check whether the machineview or tapeview was touched"
        if touch.y > 100:
            return
        self.touched = True
    
    def on_touch_move(self, touch):
        "ignore this method if the tapeview wasn't touched"
        if not self.touched:
            return
        print "move"
    
    def on_touch_up(self, touch):
        """
        opens a popup to change the tape if the tape was touched
        """
        if not self.touched:
            return
        content = TextInput(text=self.ids.TAPE.text)
        popup = Popup(title='Tape', content=content, size_hint=(0.9,0.5))
        popup.bind(on_dismiss=self.write_tape)
        popup.open()
        self.touched = False
        
    def write_tape(self, instance):
        """
        writes a new tape, whatever the user inpus
        """
        "handle left over spaces and *'s"
        text = instance.content.text
        text.replace('*', '')
        text.replace(' ', '')
        text = [ digit for digit in text ]
        "replace the tape"
        self.tape.replaceTape(text)
        "draw itself again to display the new tape"
        self.draw()
    
    def update(self):
        "todo"
        return
    
    def draw(self):
        """
        displays the contents of the tape
        """
        text = self.tape.getTape()
        self.ids.TAPE.text = text
    
    