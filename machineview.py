from state import State
from transitionpopup import TransitionPopup
from turingmachine import TuringMachine

from kivy.uix.floatlayout import FloatLayout
from kivy.graphics import Color, Ellipse, Rectangle, Line
from kivy.uix.popup import Popup
from statepopup import StatePopup
from changes import Changes

class MachineView(FloatLayout):
    "contains the turing machine"
    machine = None
    changes = Changes()
    "drawing parameters for the canvas, offset controls how far the person has dragged the canvas in panning"
    touch_coords = (0,0)
    offset = (0,0)
    "user selected state by touch"
    state = None
    "booleans to control how the program responds to user input"
    touched = False
    edit = True
    draw_trans = False
    collide = False
    new_state = False
    double = False
    
    def on_touch_down(self, touch):
        #handles touch down events using the booleans
        "checks whether the machineview or the tapeview was touched"
        if touch.y < 100:
            return
        self.touched = True
        if touch.is_double_tap:
            self.double = True
        "grabs the touch coordinates relative to the canvas offset"
        self.touch_coords = (touch.x - self.offset[0], touch.y - self.offset[1])
        "identify any collisions"
        self.state = self.collision(self.touch_coords)
        "program behaviour logic"
        if not self.edit:
            self.touch_coords = self.touch_coords
        elif not self.collide:
            self.new_state = True
            state = State(centre = (self.touch_coords), size = (50,50), color = Color(0,0,1))
            self.state = state
            self.addState(state)
            self.draw()    
    
    def on_touch_move(self, touch):
        "ignores this method if the machine view wasn't touched"
        if not self.touched:
            return
        elif not self.edit:
            if self.collide:
                self.dragState(touch)
            else:
                self.pan(touch)
        elif (touch.x,touch.y) != self.touch_coords and not self.double:
            self.draw_trans = True
            self.drawTransition(touch)
    
    def on_touch_up(self, touch):
        if not self.touched:
            return
        "adds new transition if necessary"
        touch_coords = (touch.x - self.offset[0], touch.y - self.offset[1])
        state = self.collision(touch_coords)
        if state is not None:
            if self.double:
                popup = StatePopup(state)
                popup.bind(on_dismiss=self.stateUpdate)
                popup.open()
            elif self.draw_trans:
                popup = TransitionPopup()
                popup.state = state
                popup.bind(on_dismiss=self.addTrans)
                popup.open()
            elif not self.new_state:
                state.toggleFinal()
            self.draw()
        "reset selection and behaviour variables ready for next touch"
        self.collide = False
        self.draw_trans = False
        self.touched = False
        self.new_state = False
        self.double = False
        self.draw()
    
    def undo(self):
        info = self.changes.undo()
        if info is not None:
            case = info[0]
            info = info[1]
            if case == 0:
                self.machine.delState(info)
            else:
                state = info[0]
                del state.transitions[info[2]]
            self.draw()
    
    def redo(self):
        info = self.changes.redo()
        if info is not None:
            case = info[0]
            info = info[1]
            if case == 0:
                self.addState(info)
            else:
                self.readdTrans(*info)
    
    def collision(self, touch_coords):
        for state in self.machine.states:
            if state.collision(touch_coords):
                self.collide = True
                return state
    
    def addState(self, state):
        self.machine.addState(state)
        self.changes.addChange(0, state)
        self.draw()
    
    def addTrans(self, instance):
        state = instance.state
        if instance.add:
            read, write, move = instance.ids.read.text, instance.ids.write.text, instance.ids.move.text
            self.state.addTrans(state, read, write, move)
            self.changes.addChange(1, (self.state, state, read, write, move))
            self.draw()
    
    def readdTrans(self, cur_state, new_state, read, write, move):
        cur_state.addTrans(new_state, read, write, move)
        self.draw()
    
    def drawTransition(self, touch):
        self.draw()
        canvas = self.canvas
        canvas.add(Line(points=(self.state.centre[0]+self.offset[0], self.state.centre[1]+self.offset[1],touch.x,touch.y), width = 1))
    
    def dragState(self, touch):
        drag_coords = (touch.x-self.touch_coords[0]), (touch.y-self.touch_coords[1])
        self.draw()
        self.state.drag(drag_coords)
    
    def pan(self, touch):
        self.offset = touch.x-self.touch_coords[0], touch.y-self.touch_coords[1]
        self.draw()
        
    def stateUpdate(self, instance):
        state = instance.state
        state.name = instance.ids.name.text
        if instance.initial:
            self.machine.changeInitial(state)
        if instance.delete:
            self.machine.delState(state)
        self.draw()
    
    def draw(self):
        "canvas drawing"
        canvas = self.canvas
        "clears old canvas so artifacts don't remain"
        canvas.clear()
        canvas.add(Color(0.5, 0.79, 0.58, 1))
        "the background"
        canvas.add(Rectangle(pos=self.pos, size = self.size))
        "draw machine"
        self.machine.draw(canvas, self.offset)