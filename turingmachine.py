from kivy.graphics import Color

class TuringMachine():
    #Turing machine class containing all aspects of a standard turing machine
    def __init__(self):
        "Alphabet defaults to empty"
        self.alphabet = ""
        "Blank machine has no current state"
        self.initial_state = None
        self.state = None
        "Container of all states, default empty"
        self.states = []
        self.log = []
    
    "makes a transition"
    def trans(self, tape):
        "read from tape"
        sym = tape.getSym()
        "lookup current states transitions"
        if sym in self.state.transitions:
            "transition to next state"
            self.state.current = False
            "get new values"
            state, write, move = self.state.transitions[sym]
            self.log.append((self.state, move, sym))
            self.state = state
            self.state.current = True
            "write new symbol to tape"
            tape.write(write, move)
            "return machine isn't stopped"
            return None
        "if no transition was found, return whether current state is final"
        return self.state.is_final()
    
    def undo(self, tape):
        if self.log == []:
            return
        self.state.current = False
        self.state, move, sym = self.log.pop()
        self.state.current = True
        if move == 'R':
            tape.pos -= 1
        elif move == 'L':
            tape.pos += 1
        tape.tape[tape.pos] = sym
    
    def addState(self, state):
        """
        adds a new state top the turing machine, checkes if its the first one
        """
        self.states.append(state)
        "check is there is an initial state"
        if self.initial_state is None:
            "color the initial state differently"
            state.color = Color(0,0.3,0,1)
            state.current = True
            self.initial_state = state
            "set initial to current"
            self.state = state
    
    def delState(self, state):
        """
        deletes a state and runs through existing states transitions to find link to
        the deleted state
        """
        "remove the state"
        self.states.remove(state)
        "check current and initial states if they need to be changed"
        if self.initial_state == state:
            if len(self.states) > 0:
                self.initial_state = self.states[0]
                if self.state == state:
                    self.state = self.initial_state
            else:
                self.initial_state = None
                if self.state == state:
                    self.state == None
        "remove any transitions pointing to it"
        for s in self.states:
            s.delTrans(state)
    
    def draw(self, canvas, offset):
        #draws the turing machine to the canvas
        if self.initial_state is not None:
            "make sure initial is coloured differently"
            self.initial_state.color = Color(0,0.3,0,1)
        for i in range(len(self.states)):
            "draw each state"
            self.states[i].draw(canvas, offset)
    
    def changeInitial(self, state):
        """
        changes the initial state, handling any other changes that also need to be made
        """
        self.initial_state.current = False
        self.initial_state.color = Color(0,0,0)
        state.current = True
        state.color = Color(0,0.3,0)
        self.initial_state = state
        self.state = state