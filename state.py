from kivy.graphics import Color, Ellipse, Rectangle, Line
import time
state_color = Color(0,0,0,1)
initial_color = Color(0,0.3,0,0)
accept_color = Color(0.69,0.27, 0.62,0.6)
current_color = Color(1,1,1,1)

class State(object):
    def __init__(self, **kwargs):
        self.centre = kwargs['centre']
        self.size = kwargs['size']
        self.color = state_color
        self.name = ""
        self.transitions = {}
        self.current = False
        self.final = False
    
    def draw(self, canvas, offset):
        """
        draws the state and any other notifyers informing the user on what
        the state is
        """
        "places the state in the correct position"
        centre = (offset[0]+self.centre[0], offset[1]+self.centre[1])
        if self.final:
            "add a background circle over accepting states"
            canvas.add(accept_color)
            canvas.add(Ellipse(size = (self.size[0]*5/4, self.size[1]*5/4), pos = (centre[0]-self.size[0]*5/8, centre[1]-self.size[1]*5/8)))
        "add the states circle"
        canvas.add(self.color)
        canvas.add(Ellipse(size = self.size, pos = (centre[0]-self.size[0]/2, centre[1]-self.size[0]/2)))
        if self.current:
            "add a white circle over current"
            canvas.add(current_color)
            canvas.add(Ellipse(size = (self.size[0]*2/3, self.size[1]*2/3), pos = (centre[0]-self.size[0]/3, centre[1]-self.size[0]/3)))
        "draw each of the states transitions"
        self.draw_trans(canvas, offset)
    
    def toggleFinal(self):
        "do I really need to say"
        self.final = not self.final
    
    def draw_trans(self, canvas, offset):
        """
        draws a given outgoing transition of the state
        """
        for trans in self.transitions:
            "find the transitions next state"
            next_state = self.transitions[trans][0]
            "draws a line between the two states"
            canvas.add(Line(points=(self.centre[0]+offset[0], self.centre[1]+offset[1], next_state.centre[0]+offset[0], next_state.centre[1]+offset[1])))
    
    def drag(self, drag):
        """
        DOESN"T WORK, DON"T KNOW WHY
        """
        self.centre = (drag[0]-self.centre[0], drag[1]-self.centre[1])
    
    def collision(self, touch_coords):
        "detects if a touch is whithing the circles bound"
        r = ((self.centre[0] - touch_coords[0])/(self.size[0]/2))**2 + ((self.centre[1] - touch_coords[1])/(self.size[1]/2))**2
        return r <= 1
    
    def addTrans(self, new_state, read, write, move):
        """
        adds a transition to the transitions dictionary with read being the key
        and the value being a tuple of new_state, write, move
        """
        self.transitions[read] = new_state, write, move
        print self.transitions
    
    def delTrans(self, state):
        for key in self.transitions:
            if self.transitions[key][0] == state:
                del self.transitions[key]
    
    def is_final(self):
        return self.final