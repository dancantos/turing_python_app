from main import *
from state import State
import xml.etree.ElementTree as ET
import glob, os

def saveMachine(TuringMachine, Tape, filename):
    """
    Saves the current turingmachine as is, including all states, transitions and current state.
    """
    root = ET.Element(str(filename) + "_machine")
    tree = ET.ElementTree(root)
    root.tag = "turingMachine"
    
    "Save alphabet"
    alphabet = ET.SubElement(root, "alphabet")
    alphabet.text = TuringMachine.alphabet
    
    "Save blank character"
    blank_char = ET.SubElement(root, "blank")
    blank_char.set("blank_char", Tape.blank_char)
    
    "Save current tape contents"
    tape_contents = ET.SubElement(root, "tape")
    tape_contents.text = str(Tape)
    
    "Saving states"
    initialstate = ET.SubElement(root, "initialstate")
    finalstates = ET.SubElement(root, "finalstates")
    states = ET.SubElement(root, "states")
    given_name = 0
    
    "give every state a name if it doesn't already have one"
    for state in TuringMachine.states:
        if state.name == "":
            state.name = str(given_name)
            given_name += 1
            
    for state in TuringMachine.states:
        "create new state element"
        new_element = ET.SubElement(states, "state")
        "stores states information"
        new_element.set("name", state.name)
        new_element.set("current", str(state.current))
        new_element.set("final", str(state.final))
        new_element.set("pos_x", str(state.centre[0]))
        new_element.set("pos_y", str(state.centre[1]))
        
        "saves a final state"
        if state.is_final():
            finalstate = ET.SubElement(finalstates, "finalstate")
            finalstate.set("name", state.name)
        
        transits = state.transitions
        for key in transits:
            "creates new transition element"
            trans_element = ET.SubElement(new_element, "transition")
            "stores transition information"
            trans_element.set("seensym", key)
            trans_element.set("writesym", transits[key][1])
            trans_element.set("newstate", transits[key][0].name)
            trans_element.set("move", transits[key][2])
    
    "Saving Initial State"
    initialstate.set("name", TuringMachine.initial_state.name)
    
    "Write to file"
    path = os.path.abspath(__file__)
    dname = os.path.dirname(path)
    os.chdir(dname)
    if not os.path.exists('Machines'):
        os.mkdir('Machines')
    os.chdir('Machines')
    tree.write(filename)
    
def loadMachine(filename):
    "loading file"
    path = os.path.abspath(__file__)
    dname = os.path.dirname(path)
    os.chdir(dname)
    if not os.path.exists('Machines'):
        print "Machines directory not found"
    os.chdir('Machines')
    machine_file = None
    for file in glob.glob("*.xml"):
        if file == filename:
            machine_file = ET.parse(file)
            root = machine_file.getroot()
    if machine_file is None:
        print "file not found"
        return
    machine_loaded = TuringMachine()
    tape_loaded = Tape()
    
    "load alphabet"
    alphabet = root.find('alphabet').text
    machine_loaded.alphabet = alphabet
    tape_loaded.alphabet = alphabet
    
    "load tape contents"
    tape = root.find('tape').text
    tape_loaded.replaceTape(tape)
    
    "load states into machine"
    names = {}
    states = root.find('states')
    for st in states.iter('state'):
        "creates a new state"
        state = State(centre = (0,0), size = (50,50), color = Color(0,0,1))
        "loads a states values from the file element"
        state.centre = float(st.get('pos_x')), float(st.get('pos_y'))
        state.current = False if st.get('current') == 'False' else True
        state.final = False if st.get('final') == 'False' else True
        name = st.get('name')
        "stores the state in a dictionary indexed by name to be used later"
        state.name = name
        names[name] = state
        "put state in machine"
        machine_loaded.states.append(state)
    
    "load the transitions into the states"
    for st in states.iter('state'):
        name = st.get('name')
        state = names[name]
        for tr in st.iter('transition'):
            "load values into the transition"
            read = tr.get('seensym')
            write = tr.get('writesym')
            move = tr.get('move')
            "find the state in the dictionary by name and load it"
            newstate = names[tr.get('newstate')]
            "creat the transtions"
            state.transitions[read] = newstate, write, move
    
    "load the initial state"
    initstate = root.find('initialstate').get('name')
    machine_loaded.initial_state = names[initstate]
    machine_loaded.state = names[initstate]
    
    "returns the machine and tape object to be used by the app"
    return machine_loaded, tape_loaded

