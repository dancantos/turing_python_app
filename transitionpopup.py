from kivy.uix.popup import Popup

class TransitionPopup(Popup):
    def __init__(self):
        super(TransitionPopup, self).__init__()
        self.state = None
        self.add = None
    
    "determine if a transition is to be added or discarded"
    def addTrans(self):
        self.add = True
        self.dismiss()