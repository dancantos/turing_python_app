"""
Created: 28/4/15
Authors: Daniel Cantos and Jake Krelle
"""
__version__ = '0.1'

from machineview import MachineView
from tapeview import TapeView
from turingmachine import TuringMachine
from savingandloading import *
from tape import Tape
from menu import Menu
from changes import Changes

from kivy.app import App
from kivy.uix.widget import Widget
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label
from kivy.uix.scrollview import ScrollView
from kivy.uix.textinput import TextInput
from kivy.graphics import Color, Ellipse, Rectangle, Line
from kivy.uix.popup import Popup
from kivy.uix.button import Button
from collections import deque
import time
import os

class AppView(FloatLayout):
    "initialise the app"
    def __init__(self):
        super(AppView, self).__init__()
        self.machineview = self.ids.MACHINEVIEW
        self.tapeview = self.ids.TAPEVIEW
        MachineView.machine = TuringMachine()
        TapeView.tape = Tape()
        self.speeds = deque((1, 0.5, 0.2, 100, 5, 2))
        self.edit = True
    
    "call when menu needs to be openned"
    def menu(self):
        menu = Menu()
        menu.bind(on_dismiss=self.menuClose)
        menu.open()
    
    "handle action after menu close"
    def menuClose(self, instance):
        if instance.new_machine == True:
            MachineView.machine = TuringMachine()
            TapeView.tape = Tape()
            self.draw()
        
        "if save was pressed"
        if instance.save == True:
            content = TextInput(text='filename')
            popup = Popup(title='filename', content=content, size_hint=(0.5,0.2))
            popup.bind(on_dismiss=self.save)
            popup.open()
         
        "if load was pressed"    
        if instance.load == True:
            content = TextInput(text='filename')
            popup = Popup(title='filename', content=content, size_hint=(0.5,0.2))
            popup.bind(on_dismiss=self.load)
            popup.open()
    
    "popup requesting filename"
    def save(self, instance):
        filename = instance.content.text
        saveMachine(MachineView.machine, TapeView.tape, filename + ".xml")
    
    "popup requesting filename"
    def load(self, instance):
        filename = instance.content.text
        MachineView.machine, TapeView.tape = loadMachine(filename + ".xml")
        MachineView.changes = Changes()
        self.draw()
    
    "toggles whether the user is editing or dragging"
    def toggleEditDrag(self):
        self.edit = not self.edit
        MachineView.edit = self.edit
        self.ids.toggle.text = "Edit" if self.edit else "Drag"
    
    "makes 1 step of the turing machine"
    def step(self):
        status = MachineView.machine.trans(TapeView.tape)
        if status:
            self.halt()
        elif status == False:
            self.crash()
        self.draw()
    
    def reverse(self):
        MachineView.machine.undo(TapeView.tape)
        self.draw()
    
    "runs the turing machine to completion"
    def run(self):
        status = None
        "claculating the time per step from steps per second"
        speed = 1.0/self.speeds[0]
        while status == None:
            status = MachineView.machine.trans(TapeView.tape)
            self.draw()
            time.sleep(speed)
        if status:
            self.halt()
        else:
            self.crash()
    
    "loops over the possible speeds in self.speeds"
    def toggleSpeed(self):
        self.speeds.rotate()
        "display the ned speed"
        MachineView.machine.speed = self.speeds[0]
        self.ids.SPEED.text = "speed: " + str(self.speeds[0])
    
    "popup saking for a new alphabet"
    def changeAlphabet(self):
        content = TextInput(text='alphabet')
        popup = Popup(title='Alphabet', content=content, size_hint=(0.5,0.2))
        popup.bind(on_dismiss=self.alphabet)
        popup.open()
    
    "changes the alphabet for the turing machine and tape"
    def alphabet(self, instance):
        "gets the string the user entered"
        string = instance.content.text
        "change the machines' and tapes' alphabets"
        MachineView.machine.alphabet = string
        TapeView.tape.alphabet = string
        TapeView.tape.blank_char = string[0]
        "display the new alphabet on the button"
        self.ids.ALPHABET.text = "alphabet: {}".format(string)
    
    "popup notifying of a halt"
    def halt(self):
        popup = Popup(title='Machine Halted Correctly', size_hint=(0.5,0.2))
        popup.open()
        return
    
    "popup notifying of a crash"
    def crash(self):
        popup = Popup(title='Machine Crashed', size_hint=(0.5,0.2))
        popup.open()
        return
    
    "draw mathod when needed"
    def draw(self):
        self.machineview.draw()
        self.tapeview.draw()
    
    "call undo when button pressed"
    def undo(self):
        self.machineview.undo()
    
    "call redo when button pressed"
    def redo(self):
        self.machineview.redo()

class Mathison(App):
    def build(self):
        return AppView()
        #return Button(text="Hello")

if __name__ == '__main__':
    Mathison().run()