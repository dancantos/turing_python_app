from kivy.uix.popup import Popup
import state

class StatePopup(Popup):
    def __init__(self, state):
        super(StatePopup, self).__init__()
        self.state = state
        self.initial = False
        self.delete = False
    
    def makeInitial(self):
        self.initial = True
        self.dismiss()
    
    def deleteState(self):
        self.delete = True
        self.dismiss()