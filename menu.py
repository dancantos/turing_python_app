from kivy.uix.popup import Popup

class Menu(Popup):
    def __init__(self):
        super(Menu, self).__init__()
        "booleans to determine what heppens when the popup closes"
        self.new_machine = False
        self.save = False
        self.load = False
    
    "all methods simply set their corresponding variable to true and closes the popup"
    def newMachine(self):
        self.new_machine = True
        self.dismiss()
    def saveMachine(self):
        self.save = True
        self.dismiss()
    def loadMachine(self):
        self.load = True
        self.dismiss()