class Changes:
    def __init__(self):
        self.log = []
        self.pos = 0
    
    def addChange(self, type, info):
        if self.pos not in range(len(self.log)):
            self.log.append((type, info))
        else:
            self.log[self.pos] = (type, info)
        print self.log
        self.pos += 1
    
    def undo(self):
        if self.pos == 0:
            return None
        self.pos -= 1
        return self.log[self.pos]
    
    def redo(self):
        if self.pos == len(self.log):
            return None
        self.pos += 1
        return self.log[self.pos-1]
        