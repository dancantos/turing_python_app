from collections import deque

class Tape:
    #Implements a turing machine tape that is infinite in both directions
    def __init__(self, input_string = "", blank_char = "_"):
        "When a new tape is created, set contents to be user input string or the blank character if input string not available"
        self.tape=deque(input_string) if input_string else deque(blank_char)
        "position defaults to 0"
        self.pos=0
        "define the blank character"
        self.blank_char = blank_char
    
    def __str__(self):
        return ''.join(self.tape)
    
    def replaceTape(self, char_list):
        self.tape = deque(char_list)
    
    def getSym(self):
        "Returns the symbol underneath the head"
        return self.tape[self.pos]
    
    def write(self, newchar, move):
        self.tape[self.pos] = newchar
        if move == "R":
            self.writeRight()
        elif move == "L":
            self.writeLeft()
    
    def writeLeft(self):
        #Writes new character to the tape, moves left
        "If head is already at left limit, increase tape size left"
        if self.pos == 0:
            self.tape.appendleft(self.blank_char)
        else:
            "There is space to move, move tape left"
            self.pos -= 1

    def writeRight(self):
        #Writes new character to the tape, moves right
        "Move tape right"
        self.pos += 1
        "If the tape is at its limit, increase tape size right"
        if self.pos == len(self.tape):
            self.tape.append(self.blank_char)
        

    def getTape(self):
        #Returns array of tape contents
        tape = list(self.tape)
        return ''.join(tape[:self.pos] + ['*' + tape[self.pos] + '*'] + tape[self.pos + 1:])
    